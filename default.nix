{ pkgs ? import <nixpkgs> {} }:
let 
  drv = pkgs.haskellPackages.callCabal2nix "hasker" ./. {};
in
  if pkgs.lib.inNixShell then drv.env else drv


