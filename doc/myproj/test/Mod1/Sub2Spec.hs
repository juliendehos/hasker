
module Mod1.Sub2Spec (main, spec) where

import Test.Hspec
import Test.QuickCheck

import Mod1.Sub2

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "Mod1.Sub2Spec" $ do
        it "TODO" $ property $ \x -> add42 x == (x+42)
