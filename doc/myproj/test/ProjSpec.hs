
module ProjSpec (main, spec) where

import Test.Hspec
import Test.QuickCheck

import Proj

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "ProjSpec" $ do
        it "TODO" $ property $ \x -> add42 x == (x+42)
