
{-# LANGUAGE OverloadedStrings #-}

module Hasker.BackSpec (main, spec) where

import Test.Hspec
import Test.QuickCheck

import Hasker.Back
import qualified Data.Text as T
import Data.Text.Arbitrary ()

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "toUpperFirst" $ do
    it "toto" $ toUpperFirst "toto" `shouldBe` "Toto"
    it "Toto" $ toUpperFirst "Toto" `shouldBe` "Toto"
    it "totO" $ toUpperFirst "totO" `shouldBe` "TotO"
    it "TotO" $ toUpperFirst "TotO" `shouldBe` "TotO"
    it "t" $ toUpperFirst "t" `shouldBe` "T"
    it "T" $ toUpperFirst "T" `shouldBe` "T"
    it "[]" $ toUpperFirst "" `shouldBe` ""
    -- it "length" $ property $ \x -> T.length (toUpperFirst x) == T.length x

