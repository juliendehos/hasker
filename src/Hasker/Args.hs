
module Hasker.Args where

import Prelude hiding (FilePath)
import Turtle

data Cmd
    = CmdInit Text
    | CmdLib Text
    | CmdApp Text
    deriving (Eq, Show)

data Args = Args 
    { argsRoot  :: FilePath
    , argsCmd   :: Cmd
    } deriving (Eq, Show)

