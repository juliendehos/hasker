
{-# LANGUAGE OverloadedStrings #-}

module Hasker.Gen where

import Turtle

-- init

genCabal :: Text -> Text -> Text
genCabal projName libName
    =  "\nname:               " <> projName
    <> "\nversion:            0.1"
    <> "\nlicense:            MIT"
    <> "\nbuild-type:         Simple"
    <> "\ncabal-version:      >=1.10"
    <> "\n"
    <> "\nlibrary"
    <> "\n    hs-source-dirs:     src"
    <> "\n    exposed-modules:    " <> libName
    <> "\n    ghc-options:        -Wall -O2"
    <> "\n    default-language:   Haskell2010"
    <> "\n    build-depends:      base"
    <> "\n"
    <> "\ntest-suite spec"
    <> "\n    main-is:            Spec.hs"
    <> "\n    hs-source-dirs:     test"
    <> "\n    type:               exitcode-stdio-1.0"
    <> "\n    ghc-options:        -Wall"
    <> "\n    default-language:   Haskell2010"
    <> "\n    build-depends:      base, QuickCheck, hspec, " <> projName
    <> "\n    other-modules:      " <> libName <> "Spec"
    <> "\n"

genSetup :: Text
genSetup 
    =  "\nimport Distribution.Simple"
    <> "\nmain = defaultMain"
    <> "\n"

genTestSpec :: Text
genTestSpec = "\n{-# OPTIONS_GHC -F -pgmF hspec-discover #-}\n"

genStack :: Text
genStack = "\nresolver: lts-12.26\n"

genNix :: Text -> Text
genNix projName
    =  "\n{ pkgs ? import <nixpkgs> {} }:"
    <> "\nlet"
    <> "\n  drv = pkgs.haskellPackages.callCabal2nix \"" <> projName <> "\" ./. {};"
    <> "\nin"
    <> "\n  if pkgs.lib.inNixShell then drv.env else drv"
    <> "\n"

-- app

genApp :: Text -> Text
genApp appName
    =  "\nmain :: IO ()"
    <> "\nmain = putStrLn \"TODO implement " <> appName <> "\""
    <> "\n"

genCabalApp :: Text -> Text -> Text
genCabalApp projName appName
    =  "\nexecutable " <> appName
    <> "\n    main-is:            Main.hs"
    <> "\n    hs-source-dirs:     app/" <> appName  -- TODO (hasker) check appName
    <> "\n    ghc-options:        -Wall -O2"
    <> "\n    default-language:   Haskell2010"
    <> "\n    build-depends:      base, " <> projName
    <> "\n"

-- lib

genModule :: Text -> Text
genModule moduleName
    =  "\nmodule " <> moduleName <> " where"
    <> "\n"
    <> "\nadd42 :: Int -> Int"
    <> "\nadd42 = (+42)"
    <> "\n"

genModuleSpec :: Text -> Text
genModuleSpec moduleName
    =  "\nmodule " <> moduleName <> "Spec (main, spec) where"
    <> "\n"
    <> "\nimport Test.Hspec"
    <> "\nimport Test.QuickCheck"
    <> "\n"
    <> "\nimport " <> moduleName
    <> "\n"
    <> "\nmain :: IO ()"
    <> "\nmain = hspec spec"
    <> "\n"
    <> "\nspec :: Spec"
    <> "\nspec = do"
    <> "\n    describe \"" <> moduleName <> "Spec\" $ do"
    <> "\n        it \"TODO\" $ property $ \\x -> add42 x == (x+42)"
    <> "\n"

