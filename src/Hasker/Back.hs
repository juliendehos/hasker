
{-# LANGUAGE OverloadedStrings #-}

module Hasker.Back where

import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import Hasker.Args
import Hasker.Gen
import Prelude hiding (FilePath)
import Turtle

-- utils

myEncodeString :: FilePath -> Text -> Text
myEncodeString path suff = case toText path of
    Left ee -> ee
    Right p -> p <> suff

myWriteFile :: FilePath -> Text -> IO ()
myWriteFile fullpath content = do
    exist <- testfile fullpath
    if exist
    then TIO.putStrLn $ myEncodeString fullpath "... skipped"
    else do
        mktree $ directory fullpath
        writeTextFile fullpath content
        TIO.putStrLn $ myEncodeString fullpath "... created"

findProjName :: FilePath -> Shell FilePath
findProjName rootPath = basename <$> find (ends ".cabal") rootPath

toUpperFirst :: Text -> Text
toUpperFirst str = T.append (T.toUpper s1) s2
    where (s1, s2) = T.splitAt 1 str

-- main run functions

run :: Args -> IO ()
run (Args rootPath cmd) = runCmd rootPath cmd

runCmd :: FilePath -> Cmd -> IO ()
runCmd rootPath (CmdInit projName) = runInit rootPath projName
runCmd rootPath (CmdLib moduleName) = runLib rootPath moduleName 
runCmd rootPath (CmdApp appName) = runApp rootPath appName

-- init

runInit :: FilePath -> Text -> IO ()
runInit rootPath projName = do
    let libName = toUpperFirst projName
        libNamePath = fromText $ libName
        projPath = fromText projName
        specPath = fromText (libName <> "Spec")
    myWriteFile (rootPath </> projPath <.> "cabal") (genCabal projName libName)
    myWriteFile (rootPath </> "default.nix") (genNix projName)
    myWriteFile (rootPath </> "Setup.hs") genSetup
    myWriteFile (rootPath </> "stack.yaml") genStack
    myWriteFile (rootPath </> "test/Spec.hs") genTestSpec
    myWriteFile (rootPath </> "test" </> specPath <.> "hs") (genModuleSpec libName)
    myWriteFile (rootPath </> "src" </> libNamePath <.> "hs") (genModule libName)

-- app

runApp :: FilePath -> Text -> IO ()
runApp rootPath appName = do
    let srcFullpath = rootPath </> "app" </> fromText appName </> "Main.hs"
    myWriteFile srcFullpath (genApp appName)
    updateCabalApp rootPath appName

updateCabalApp :: FilePath -> Text -> IO ()
updateCabalApp rootPath appName = sh $ do
    projNamePath <- findProjName rootPath
    let cabalFile = rootPath </> projNamePath <.> "cabal"
        projName = format fp projNamePath
        appPattern = prefix "executable" <> spaces1 <> contains (text appName)
    grepRes <- strict $ grep appPattern (input cabalFile)
    if grepRes == ""
    then do
        mapM_ (append cabalFile . return) (textToLines $ genCabalApp projName appName)
        printf (fp%"... updated\n") cabalFile
    else printf (fp%"... skipped\n") cabalFile

-- lib

runLib :: FilePath -> Text -> IO ()
runLib rootPath moduleName = do
    let moduleFilename = T.replace "." "/" moduleName
        moduleFullPath = rootPath </> "src" </> fromText moduleFilename <.> "hs"
        specFullPath = rootPath </> "test" </> fromText (moduleFilename <> "Spec") <.> "hs"
    myWriteFile moduleFullPath (genModule moduleName) 
    myWriteFile specFullPath (genModuleSpec moduleName) 
    updateCabalLib rootPath moduleName

updateCabalLib :: FilePath -> Text -> IO ()
updateCabalLib rootPath moduleName = sh $ do
    projNamePath <- findProjName rootPath
    let cabalFile = rootPath </> projNamePath <.> "cabal"
    inplace (addSuffix "exposed-modules" moduleName) cabalFile
    inplace (addSuffix "other-modules" (moduleName <> "Spec")) cabalFile
    printf (fp%"... updated\n") cabalFile
    -- TODO (hasker) check comma

addSuffix :: Pattern Text -> Text -> Pattern Text
addSuffix patt suff = do
    pp <- patt <> star anyChar
    if T.isInfixOf suff pp 
    then return pp
    else return (pp <> ", " <> suff)

