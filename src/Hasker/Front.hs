
{-# LANGUAGE OverloadedStrings #-}

module Hasker.Front where

import Hasker.Args
import Options.Applicative
import Data.Monoid

argsParser :: Parser Args
argsParser = Args
    <$> strOption 
        (long "root" <> showDefault <> value "." <> metavar "project_root")
    <*> cmdParser

cmdParser :: Parser Cmd
cmdParser = subparser
    (  command "init" (info cmdInit (progDesc "Init a project"))
    <> command "lib" (info cmdLib (progDesc "Add a module in the library"))
    <> command "app" (info cmdApp (progDesc "Add an application"))
    )

cmdInit :: Parser Cmd
cmdInit = CmdInit <$> argument str (metavar "project_name")

cmdLib :: Parser Cmd
cmdLib = CmdLib <$> argument str (metavar "module_name")

cmdApp :: Parser Cmd
cmdApp = CmdApp <$> argument str (metavar "application_name")

get :: IO (Args)
get = execParser
    $ info (argsParser <**> helper)
           (fullDesc <> progDesc "HASKell project managER")

