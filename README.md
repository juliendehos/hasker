# hasker

*This project is still experimental.*

Hasker (HASKell project managER) is a small command-line tool for creating a
Haskell project. It implements 3 commands: `init` (initialize a project), `app`
(add an executable in the project) and `lib` (add a module in the library of
the project and add a test file for this module).  The generated project can be
built using Nix+Cabal or using Stack. 


## Installation

- using Nix:

```
nix-env -i -f https://gitlab.com/juliendehos/hasker/-/archive/master/hasker-master.tar.gz
```

- using Stack:

```
stack install

# then add $HOME/.local/bin to your PATH:
echo "export PATH=$PATH:$HOME/.local/bin" >> ~/.bashrc
source ~/.bashrc
```


## Usage

- get help:

```
hasker --help
```

- create a project:

```
hasker init proj --root myproj
cd myproj
hasker app app1
hasker lib Mod1.Sub1
...
```

- build the project using Nix+Cabal:
q
```
nix-shell
cabal build
...
```

- build the project using Stack:

```
stack build
...
```

- example (see the [resulting project](./doc/myproj)):

![](./doc/hasker.mp4)


